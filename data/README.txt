google-government-detailed-removal-requests.csv

This csv file contains the information regarding removals broken down by
requester, the reporting format we started using in 2019. For each requester we
provide insight into  item level actions or status using the same categories
that we employ in the report.



google-government-removal-requests.csv

This file contains detailed information regarding requests we received by
country and by product.



historical-folder

This folder includes the csv files with the reporting format we used through
2018. These reports include breakdowns by Judicial and Executive branches, along
with compliance numbers.



google-government-removal-requests-historical.csv

This file contains the information for all content removal requests displayed in
the Government requests to remove content report, organized by country and
biannual reporting period.



google-government-detailed-removal-requests-historical.csv

This file contains the information for all content removal requests displayed in
the Government requests to remove content report, organized by country, biannual
reporting period, product, and the reasons cited for removal. Since we began
disclosing reasons for removal in the July–December 2010 reporting period, the
reason data is missing for reporting periods earlier than that time.



See https://transparencyreport.google.com/government-removals/overview for more
information.
